# README #

This README file provides the information about the pre-requisites and automation tests details about the confluence-app test project framework designed by Bhushan Nimgaonkar

### What is this repository for? ###

This repository is for test project of confluence app

### How do I get set up? ###

This automation fraemwork requires us to login first to the SITE URL. My atlassian site name is **bhushan013.atlassian.net** 

Test should be run through command line interface using maven commands. First we need to go the project directory and then fire the following commands.
**mvn compile** 
**mvn integration-test**

### Contribution guidelines ###

The test automation framework is designed on cucumber based framework. There are three feature files have been written to test the functionality of Login to confluence app , Creating a page and setting up the restrictions.
First Feature file is "LoginTest.feature": This feature will test the login functionality of confluence application. We can execute framework in any browser by modifying values in fature files.

Second feature file is "createPageFeature.feature: This will test the create page functionality of the confluence application

The third feature file is "pageRestrictionsTest.feature". This will test the page restrictions functionality of the confluence application.